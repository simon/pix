/* ----------------------------------------------------------------------
 * Generic command-line parsing functions.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include "cmdline.h"

bool parsestr(char *string, void *vret) {
    char **ret = (char **)vret;
    *ret = string;
    return true;
}

bool parseint(char *string, void *vret) {
    int *ret = (int *)vret;
    int i;
    i = strspn(string, "0123456789");
    if (i > 0) {
        *ret = atoi(string);
        string += i;
    } else
        return false;
    if (*string)
        return false;
    return true;
}

bool parsesignedint(char *string, void *vret) {
    int *ret = (int *)vret;
    int i, j;
    j = 0;
    if (string[j] == '+' || string[j] == '-')
        j++;
    i = strspn(string + j, "0123456789");
    if (i > 0) {
        *ret = atoi(string);
        string += i+j;
    } else
        return false;
    if (*string)
        return false;
    return true;
}

bool parsesize(char *string, void *vret) {
    struct Size *ret = (struct Size *)vret;
    int i;
    i = strspn(string, "0123456789");
    if (i > 0) {
        ret->w = atoi(string);
        string += i;
    } else
        return false;
    if (*string++ != 'x')
        return false;
    i = strspn(string, "0123456789");
    if (i > 0) {
        ret->h = atoi(string);
        string += i;
    } else
        return false;
    if (*string)
        return false;
    return true;
}

bool parseflt(char *string, void *vret) {
    double *d = (double *)vret;
    char *endp;
    *d = strtod(string, &endp);
    if (endp && *endp) {
        return false;
    } else
        return true;
}

bool parsebool(char *string, void *vret) {
    bool *d = (bool *)vret;
    if (!strcmp(string, "yes") ||
        !strcmp(string, "true") ||
        !strcmp(string, "verily")) {
        *d = true;
        return true;
    } else if (!strcmp(string, "no") ||
               !strcmp(string, "false") ||
               !strcmp(string, "nowise")) {
        *d = false;
        return true;
    }
    return false;
}

/*
 * Read a colour into an RGB structure.
 */
bool parsecol(char *string, void *vret) {
    struct RGB *ret = (struct RGB *)vret;
    char *q;

    ret->r = strtod(string, &q);
    string = q;
    if (!*string) {
        return false;
    } else
        string++;
    ret->g = strtod(string, &q);
    string = q;
    if (!*string) {
        return false;
    } else
        string++;
    ret->b = strtod(string, &q);

    return true;
}

/*
 * 'Parsing' function used with flag options, to increment an integer.
 * Used for things like -v which increase verbosity more if you
 * specify them more times.
 */
bool incrementint(char *string, void *vret)
{
    int *pi = (int *)vret;
    assert(!string);
    (*pi)++;
    return true;
}

static void process_option(char const *programname,
                           const struct Cmdline *option,
                           char *arg, void *optdata)
{
    assert((arg != NULL) == (option->arghelp != NULL));

    if (option->parse &&
        !option->parse(arg, (char *)optdata + option->parse_ret_off)) {
        fprintf(stderr, "%s: unable to parse %s `%s'\n", programname,
                option->valname, arg);
        exit(EXIT_FAILURE);
    }
    if (option->gotflag_off >= 0)
        *(bool *)((char *)optdata + option->gotflag_off) = true;
}

void parse_cmdline(char const *programname, int argc, char **argv,
                   const struct Cmdline *options, int noptions, void *optdata)
{
    bool doing_options = true;
    int i;
    const struct Cmdline *argopt = NULL;

    for (i = 0; i < noptions; i++) {
        if (options[i].gotflag_off >= 0)
            *(bool *)((char *)optdata + options[i].gotflag_off) = false;
    }

    while (--argc > 0) {
        char *arg = *++argv;

        if (!strcmp(arg, "--")) {
            doing_options = false;
        } else if (!doing_options || arg[0] != '-') {
            if (!argopt) {
                for (i = 0; i < noptions; i++) {
                    if (!options[i].nlongopts && !options[i].shortopt) {
                        argopt = &options[i];
                        break;
                    }
                }

                if (!argopt) {
                    fprintf(stderr, "%s: no argument words expected\n",
                            programname);
                    exit(EXIT_FAILURE);
                }
            }

            process_option(programname, argopt, arg, optdata);
        } else {
            char c = arg[1];
            char *val = arg+2;
            int i, j, len;
            bool done = false;
            for (i = 0; i < noptions; i++) {
                /*
                 * Try a long option.
                 */
                if (c == '-') {
                    char *opt = options[i].longopt;
                    for (j = 0; j < options[i].nlongopts;
                         j++, opt += 1 + strlen(opt)) {
                         len = strlen(opt);
                        if (!strncmp(arg, opt, len) &&
                            len == strcspn(arg, "=")) {
                            if (options[i].arghelp) {
                                if (arg[len] == '=') {
                                    process_option(programname, &options[i],
                                                   arg + len + 1, optdata);
                                } else if (--argc > 0) {
                                    process_option(programname, &options[i],
                                                   *++argv, optdata);
                                } else {
                                    fprintf(stderr, "%s: option `%s' requires"
                                            " an argument\n", programname,
                                            arg);
                                    exit(EXIT_FAILURE);
                                }
                            } else {
                                process_option(programname, &options[i],
                                               NULL, optdata);
                            }
                            done = true;
                        }
                    }
                    if (!done) {
                        if (!strcmp(arg, "--version")) {
#ifdef VERSION
                            printf("%s version %s\n", programname, VERSION);
#else
                            printf("%s, unknown version\n", programname);
#endif
                            exit(EXIT_SUCCESS);
                        }
                    }
                } else if (c == options[i].shortopt) {
                    if (options[i].arghelp) {
                        if (*val) {
                            process_option(programname, &options[i], val,
                                           optdata);
                        } else if (--argc > 0) {
                            process_option(programname, &options[i], *++argv,
                                           optdata);
                        } else {
                            fprintf(stderr, "%s: option `%s' requires an"
                                    " argument\n", programname, arg);
                            exit(EXIT_FAILURE);
                        }
                    } else {
                        process_option(programname, &options[i], NULL,
                                       optdata);
                    }
                    done = true;
                }
            }
            if (!done && c == '-') {
                fprintf(stderr, "%s: unknown option `%.*s'\n",
                        programname, (int)strcspn(arg, "="), arg);
                exit(EXIT_FAILURE);
            }
        }
    }
}

void usage_message(char const *usageline,
                   const struct Cmdline *options, int noptions,
                   char **extratext, int nextra)
{
    int i, maxoptlen = 0;
    char *prefix;

    printf("usage: %s\n", usageline);

    /*
     * Work out the alignment for the help display.
     */
    for (i = 0; i < noptions; i++) {
        int optlen = 0;

        if (options[i].shortopt)
            optlen += 2;               /* "-X" */

        if (options[i].nlongopts) {
            if (optlen > 0)
                optlen += 2;           /* ", " */
            optlen += strlen(options[i].longopt);
        }

        if (options[i].arghelp) {
            if (optlen > 0)
                optlen++;              /* " " */
            optlen += strlen(options[i].arghelp);
        }

        if (maxoptlen < optlen)
            maxoptlen = optlen;
    }

    /*
     * Display the option help.
     */
    prefix = "where: ";
    for (i = 0; i < noptions; i++) {
        int optlen = 0;

        printf("%s", prefix);

        if (options[i].shortopt)
            optlen += printf("-%c", options[i].shortopt);

        if (options[i].nlongopts) {
            if (optlen > 0)
                optlen += printf(", ");
            optlen += printf("%s", options[i].longopt);
        }

        if (options[i].arghelp) {
            if (optlen > 0)
                optlen += printf(" ");
            optlen += printf("%s", options[i].arghelp);
        }

        printf("%*s  %s\n", maxoptlen-optlen, "", options[i].deschelp);

        prefix = "       ";
    }

    for (i = 0; i < nextra; i++) {
        printf("%s\n", extratext[i]);
    }

    exit(EXIT_FAILURE);
}
