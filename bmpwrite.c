/* ----------------------------------------------------------------------
 * Functions to write out .BMP files. Also supports PPM, because it
 * turns out to come in useful in some situations.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

#include "bmpwrite.h"
#include "pngout.h"

static void fput32(unsigned long val, FILE *fp);
static void fput16(unsigned val, FILE *fp);

struct Bitmap {
    FILE *fp;
    bool close;
    int type;
    int width, height;
    unsigned long padding;
    unsigned short *data, *dataptr;
};

imagetype infer_type(const char *progname, const char *type,
                     const char *filename)
{
    if (!type) {
        /* Automatically infer type from file extension. */
        const char *dot = strrchr(filename, '.');
        if (dot && !strcmp(dot, ".png"))
            return PNG;
        if (dot && !strcmp(dot, ".ppm"))
            return PPM;
        if (dot && !strcmp(dot, ".bmp"))
            return BMP;
        fprintf(stderr, "%s: unable to infer image type from filename '%s';"
                " use --type={png,ppm,bmp}\n", progname, filename);
        exit(1);
    }
    if (!strcmp(type, "png"))
        return PNG;
    if (!strcmp(type, "ppm"))
        return PPM;
    if (!strcmp(type, "bmp"))
        return BMP;
    fprintf(stderr, "%s: unrecognised image type '%s';"
                " use --type={png,ppm,bmp}\n", progname, type);
    exit(1);
}

static void fput32(unsigned long val, FILE *fp) {
    fputc((val >>  0) & 0xFF, fp);
    fputc((val >>  8) & 0xFF, fp);
    fputc((val >> 16) & 0xFF, fp);
    fputc((val >> 24) & 0xFF, fp);
}
static void fput16(unsigned val, FILE *fp) {
    fputc((val >>  0) & 0xFF, fp);
    fputc((val >>  8) & 0xFF, fp);
}

struct Bitmap *bmpinit(char const *filename, int width, int height,
                       imagetype type)
{
    struct Bitmap *bm;

    bm = (struct Bitmap *)malloc(sizeof(struct Bitmap));
    bm->type = type;
    bm->width = width;
    bm->height = height;

    if (filename[0] == '-' && !filename[1]) {
        bm->fp = stdout;
        bm->close = false;
    } else {
        bm->fp = fopen(filename, "wb");
        bm->close = true;
    }

    if (type == BMP) {
        /*
         * BMP File format is:
         *
         * 2char "BM"
         * 32bit total file size
         * 16bit zero (reserved)
         * 16bit zero (reserved)
         * 32bit 0x36 (offset from start of file to image data)
         * 32bit 0x28 (size of following BITMAPINFOHEADER)
         * 32bit width
         * 32bit height
         * 16bit 0x01 (planes=1)
         * 16bit 0x18 (bitcount=24)
         * 32bit zero (no compression)
         * 32bit size of image data (total file size minus 0x36)
         * 32bit 0xB6D (XPelsPerMeter)
         * 32bit 0xB6D (YPelsPerMeter)
         * 32bit zero (palette colours used)
         * 32bit zero (palette colours important)
         *
         * then bitmap data, BGRBGRBGR... with padding zeros to bring
         * scan line to a multiple of 4 bytes. Padding zeros DO happen
         * after final scan line. Scan lines work from bottom upwards.
         */
        unsigned long scanlen, bitsize;

        scanlen = 3 * width;
        bm->padding = ((scanlen+3)&~3) - scanlen;
        bitsize = (scanlen + bm->padding) * height;

        fputs("BM", bm->fp);
        fput32(0x36 + bitsize, bm->fp);
        fput16(0, bm->fp); fput16(0, bm->fp);
        fput32(0x36, bm->fp); fput32(0x28, bm->fp);
        fput32(width, bm->fp); fput32(height, bm->fp);
        fput16(1, bm->fp); fput16(24, bm->fp);
        fput32(0, bm->fp); fput32(bitsize, bm->fp);
        fput32(0xB6D, bm->fp); fput32(0xB6D, bm->fp);
        fput32(0, bm->fp); fput32(0, bm->fp);
    } else if (type == PPM) {
        /*
         * PPM file format is:
         *
         * 2char "P6"
         * arbitrary whitespace
         * ASCII decimal width
         * arbitrary whitespace
         * ASCII decimal height
         * arbitrary whitespace
         * ASCII decimal maximum RGB value (here 255, for one-byte-per-sample)
         * one whitespace character
         *
         * then simply bitmap data, RGBRGBRGB...
         */
        fprintf(bm->fp, "P6 %d %d 255\n", width, height);

        bm->padding = 0;
    } else if (type == PNG) {
        bm->data = (unsigned short *)malloc(3 * width * height *
                                            sizeof(unsigned short));
        bm->dataptr = bm->data;
        bm->padding = 0;
    } else {
        assert(0 && "bad type");
    }

    return bm;
}

void bmppixel(struct Bitmap *bm,
              unsigned char r, unsigned char g, unsigned char b) {
    if (bm->type == BMP) {
        putc(b, bm->fp);
        putc(g, bm->fp);
        putc(r, bm->fp);
    } else if (bm->type == PPM) {
        putc(r, bm->fp);
        putc(g, bm->fp);
        putc(b, bm->fp);
    } else if (bm->type == PNG) {
        *bm->dataptr++ = r;
        *bm->dataptr++ = g;
        *bm->dataptr++ = b;
    } else {
        assert(0 && "bad type");
    }
}

void bmpendrow(struct Bitmap *bm) {
    int j;
    for (j = 0; j < bm->padding; j++)
        putc(0, bm->fp);
}

void bmpclose(struct Bitmap *bm) {
    if (bm->type == PNG) {
        size_t size;
        void *pngdata = pngwrite(bm->width, bm->height, 3, 8, bm->data, &size);
        fwrite(pngdata, 1, size, bm->fp);
        free(pngdata);
        free(bm->data);
    }
    if (bm->close)
        fclose(bm->fp);
    free(bm);
}
