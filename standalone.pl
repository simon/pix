#!/usr/bin/perl 

# Create single standalone C files for the various programs here.
#
# This is absolutely horrid, but until now I've been distributing
# single C files, and it does seem like the simplest way to avoid
# having to distribute an enormous Makefile structure and
# everything...

use Getopt::Long;

my $outdir = undef;
my @programs = ();
GetOptions("o|output=s" => \$outdir,
           "help" => \$help)
    or die "standalone.pl: invalid options; try --help\n";

if ($help) {
    print <<EOF;
usage:   ./standalone.pl -o OUTDIR srcfile [srcfile...]
options: -o, --output OUTDIR     directory to write standalone output source
                                    files into
EOF
exit 0;
}

my $outpfx = $outdir;
$outpfx .= "/" if $outdir =~ m{[^/]$};

for (@ARGV) { &munge($_); }

sub blank {
    my ($line) = @_;
    return $line =~ /^\s*\n$/s;
}

sub munge {
    my ($infile) = @_;
    my $outfile = $outpfx . $infile;

    my @stdhdrs = ();
    my %stdhdrs = ();
    my @hdrs = ();
    my @sources = ();
    my @queue = ($infile);
    my %loaded = {};

    while (defined ($infile = shift @queue)) {
        my @outlines = ();
        my $dest = ($infile =~ /\.h$/ ? \@hdrs : \@sources);
        open my $fh, "<", $infile;
        while ($line = <$fh>) {
            if ($line =~ /^#include/) {
                if ($line =~ /<(.*)>/) {
                    # Avoid a couple of OS-specific headers included
                    # by deflate.c's built-in test program
                    next if {"fcntl.h"=>1, "io.h"=>1}->{$1};
                    if (!$stdhdrs{$1}) {
                        push @stdhdrs, $line;
                        $stdhdrs{$1} = 1;
                    }
                } elsif ($line =~ /^#include "([a-zA-Z0-9_]+).h"/) {
                    my $basename = $1;
                    die "missing include: $line" unless -f "$basename.h";
                    if (!$loaded{$basename}) {
                        $loaded{$basename} = 1;
                        push @queue, "$basename.h";
                        push @queue, "$basename.c" if -f "$basename.c";
                    }
                } else {
                    die "confusing include: $line" unless -f "$basename.h";
                }
            } elsif ($line =~ /^#ifndef \S+_H/ ||
                     $line =~ /^#define \S+_H/ ||
                     $line =~ /^#endif \/\* \S+_H \*\//) {
                # remove include guards
            } else {
                push @outlines, $line
                    unless @outlines and &blank($line)
                    and &blank($outlines[$#outlines]);
            }
        }
        close $fh;
        unshift @$dest,
        "/* ".("-"x20)." originally from $infile ".("-"x20)." */\n",
        "\n",
        @outlines;
    }

    my @lines = (
"/*\n",
" * This single standalone C program is not the original source form of\n",
" * this program. For convenience of distribution, it's been bolted\n",
" * together into this form by a Perl script, from a collection of more\n",
" * normally organised modules.\n",
" *\n",
" * If you'd rather see the original version, you can find it at\n",
" *   https://git.tartarus.org/simon/pix.git\n",
" */\n",
"\n", @stdhdrs, "\n", @hdrs, @sources);

    open OUT, ">$outfile";
    foreach $line (@lines) {
        print OUT $line;
    }
    close OUT;
}
