/*
 * PNG encoder.
 */

/*
 * TODO:
 *
 *  - Test thoroughly.
 *
 *  - tRNS can be shortened if the last pixels are non-transparent,
 *    so it might be nice to sort the palette to take account of
 *    that?
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <limits.h>

#include "misc.h"
#include "crc32.h"
#include "deflate.h"
#include "pngout.h"

#define PUT_32BIT_MSB_FIRST(cp, value) ( \
  (cp)[0] = (unsigned char)((value) >> 24), \
  (cp)[1] = (unsigned char)((value) >> 16), \
  (cp)[2] = (unsigned char)((value) >> 8), \
  (cp)[3] = (unsigned char)(value) )

struct pixel {
    unsigned short r, g, b, a;
};

struct interlace_pass {
    int xstart, xstep, ystart, ystep;
};

/*
 * With no interlacing, there is one pass over the image which
 * takes in all its pixels.
 */
#if 0 /* we don't actually have an option to use this at the moment */
static const struct interlace_pass null_interlace_passes[] = {
    {0, 1, 0, 1},
};
#endif

/*
 * Adam7 interlacing consists of seven passes which between them
 * specify every pixel exactly once.
 */
static const struct interlace_pass adam7_interlace_passes[] = {
    {0, 8, 0, 8},
    {4, 8, 0, 8},
    {0, 4, 4, 8},
    {2, 4, 0, 4},
    {0, 2, 2, 4},
    {1, 2, 0, 2},
    {0, 1, 1, 2},
};

static struct pixel read_pixel(const unsigned short **bitmap, int channels,
                               unsigned short mul)
{
    struct pixel ret;
    ret.r = *(*bitmap)++ * mul;
    if (channels < 3) {
        ret.g = ret.b = ret.r;
    } else {
        ret.g = *(*bitmap)++ * mul;
        ret.b = *(*bitmap)++ * mul;
    }
    if (channels & 1) {
        ret.a = 0xFFFF;
    } else {
        ret.a = *(*bitmap)++ * mul;
    }
    return ret;
}

static unsigned uabs(unsigned u)
{
    /* Return the absolute value of u, if u were interpreted as signed. */
    return u > (unsigned)INT_MAX ? -u : u;
}

/*
 * inputs:
 *  - w,h == size of image
 *  - channels = 1, 2, 3 or 4 (greyscale, grey+alpha, rgb,
 *    rgb+alpha)
 *  - depth = number of significant bits in each sample; must be 1,
 *    2, 4, 8 or 16
 *  - bitmap = array of h rows of w columns of 'channels' samples
 *    each ranging from 0 to (1<<depth)-1
 *
 * returns:
 *  - return value points to a dynamically allocated PNG in memory
 *  - *size gives the number of bytes in that PNG data
 *  - NULL return means failure to allocate memory
 */
void *pngwrite(int w, int h, int channels, int depth,
               const unsigned short *bitmap, size_t *size)
{
    struct pixel palette[256];
    int npalette;
    unsigned short mul = 0xFFFFU / ((1 << depth)-1);
    unsigned short d8, d4, d2, d1;
    const struct interlace_pass *passes;
    int npasses;
    int ochannels;
    unsigned filter_offset;
    unsigned pass_width[7], pass_height[7];
    unsigned max_width;
    size_t scandata_size, filesize;
    unsigned char *scandata, *p, *q, *filtered[5], *output;
    void *compressed;
    int complen;
    int i, j, colour, alpha, coltype, cdepth, bitdepth;
    const unsigned short *bp;
    deflate_compress_ctx *def;
    unsigned long crc;

    /*
     * Count the distinct pixel values, and build up a palette. At
     * the end of this loop we've either gone through the whole
     * image and found at most 256 pixel values which are contained
     * in 'palette', or else we've found more than 256 and have to
     * do a true-colour image.
     *
     * We also determine in this loop whether the image is
     * greyscale, whether it has a non-trivial alpha channel, and
     * (for use if we need to do true colour) what bit depth is
     * required.
     */
    colour = 0;
    alpha = 0;
    npalette = 0;
    d8 = d4 = d2 = d1 = 0;
    for (i = 0, bp = bitmap; i < w*h; i++) {
        /*
         * Read a pixel.
         */
        struct pixel pix = read_pixel(&bp, channels, mul);
        struct pixel p1, p2;

        /*
         * Check the static conditions: detect non-greyscale pixels
         * (their r, g, b components are not all identical), pixels
         * with interesting alpha values (i.e. not 0xFFFF), and
         * count the bit depth required to represent any given pixel
         * accurately.
         *
         * The last of these jobs - bit depth determination - is
         * done using some modular bit-twiddling trickery to save
         * time. The obvious approach is to observe that any 8-bit
         * sample converted to 16 bits will be an exact multiple of
         * 0x0101, so therefore we can determine if a 16-bit RGBA
         * tuple can be represented losslessly as an 8-bit one by
         * dividing each sample by 0x0101 and making sure the
         * remainder is zero. However, that involves four divisions,
         * and another four to check each of the 4-, 2- and 1-bit
         * depths, so that's all a bit unpleasant.
         *
         * A nicer approach is to use modular arithmetic. An 8-bit
         * sample converted to 16 bits will be the result of
         * multiplying a number in the range [0,255] by 0x0101
         * modulo 2^16. Multiplication by 0x0101 mod 2^16 is an
         * invertible operation (because 0x0101 is odd), and the
         * inverse operation is to multiply by 0xFF01 mod 2^16
         * (because 0xFF01 * 0x0101 = 0x1000001 which is congruent
         * to 1 mod 2^16). So we can take our sample, multiply by
         * 0xFF01, and check that no bits above the bottom 8 are
         * set. Likewise for the other depths: the multipliers for
         * 8-, 4-, 2- and 1- bit samples are respectively 0x0101,
         * 0x1111, 0x5555 and 0xFFFF, and their modular inverses are
         * 0xFF01, 0xFFF1, 0xFFFD and 0xFFFF respectively - also
         * known as -255, -15, -3 and -1. (Of course, for the last
         * of those, an actual multiplication would be overkill.)
         *
         * In fact, we streamline the process even further, by ORing
         * together the results of those multiplications across all
         * samples of all pixels, and then only checking for
         * unwanted set bits once at the very end of the loop. So
         * our variables d8, d4, d2 and d1 accumulate the bitwise
         * ORs of all those products.
         */
        if (!colour && (pix.r != pix.g || pix.r != pix.b))
            colour = 1;
        if (!alpha && pix.a != 0xFFFFU)
            alpha = 1;
        d8 |= ((pix.r * -255) | (pix.g * -255) |
               (pix.b * -255) | (pix.a * -255));
        d4 |= ((pix.r * -15) | (pix.g * -15) |
               (pix.b * -15) | (pix.a * -15));
        d2 |= ((pix.r * -3) | (pix.g * -3) |
               (pix.b * -3) | (pix.a * -3));
        d1 |= ((-pix.r) | (-pix.g) | (-pix.b) | (-pix.a));

        /*
         * Find this pixel value in the palette. We also shuffle the
         * palette upwards as we search, so that it's constantly
         * being move-to-front encoded. This hopefully arranges that
         * in typical images we take significantly less than
         * O(number of pixels * palette size) time to do this scan.
         */
        if (npalette <= 256) {
            p1 = pix;
            for (j = 0; j < npalette; j++) {
                p2 = palette[j];
                palette[j] = p1;
                if (p2.r == pix.r && p2.g == pix.g &&
                    p2.b == pix.b && p2.a == pix.a) {
                    break;
                }
                p1 = p2;
            }
            if (j == npalette) {
                if (npalette < 256)
                    palette[npalette] = p1;
                npalette++;
            }
        }
    }
    colour &= 1;                       /* normalise 'maybe' to 'no' */
    alpha &= 1;
    cdepth = (d8 & 0xFF00 ? 16 :
              d4 & 0xFFF0 ? 8 :
              d2 & 0xFFFC ? 4 :
              d1 & 0xFFFE ? 2 : 1);

    /*
     * Choose the PNG-level bit depth and colour type we'll use for
     * the real output.
     *
     * (Note that cdepth <= 8 is a second condition for using a
     * palette, because while PNG in general supports 16
     * bits/channel, its palette chunks only go up to 8 bits.)
     */
    if (npalette <= 256 && cdepth <= 8) {
        coltype = 3;                   /* paletted */
        bitdepth = (npalette <= 2 ? 1 :
                    npalette <= 4 ? 2 :
                    npalette <= 16 ? 4 : 8);
        /*
         * Special case: if we can do a pure greyscale image without
         * increasing the bit depth, do that instead, and save
         * storing a palette.
         */
        if (bitdepth == cdepth && !colour && !alpha)
            coltype = 0;
    } else {
        /* Colour types 0,2,4,6 mean grey,rgb,grey+alpha,rgba */
        coltype = 2*colour + 4*alpha;
        /* Only pure greyscale can go below 8 bit depth */
        if (coltype != 0 && cdepth < 8)
            bitdepth = 8;
        else
            bitdepth = cdepth;
    }
    ochannels = (coltype == 2 ? 3 :
                 coltype == 4 ? 2 :
                 coltype == 6 ? 4 : 1);

    /*
     * Decide on the division into scanlines (i.e. are we
     * interlacing?) and encode the unfiltered scanline data.
     *
     * Currently I fix on always interlacing, though it would be
     * easy enough to change that decision configurably.
     */
    passes = adam7_interlace_passes;
    npasses = lenof(adam7_interlace_passes);
    assert(npasses <= lenof(pass_width));
    /*
     * Compute the size of the unfiltered data. Each scanline in
     * each pass consists of the appropriate number of pixels,
     * multiplied by the number of bits per pixel, then rounded up
     * to the nearest byte, plus one prefixed byte for the filter
     * type.
     */
    scandata_size = 0;
    max_width = 0;
    for (i = 0; i < npasses; i++) {
        pass_width[i] =
            (w - passes[i].xstart + passes[i].xstep - 1) / passes[i].xstep;
        pass_width[i] *= ochannels * bitdepth;
        pass_width[i] += 7;
        pass_width[i] >>= 3;

        pass_height[i] =
            (h - passes[i].ystart + passes[i].ystep - 1) / passes[i].ystep;

        /* Empty passes don't even have the filter type byte */
        if (pass_width[i] && pass_height[i]) {
            pass_width[i]++;
            scandata_size += pass_width[i] * pass_height[i];
            if (max_width < pass_width[i])
                max_width = pass_width[i];
        }
    }
    scandata = (unsigned char *)malloc(scandata_size + 4 * max_width);
    if (!scandata)
        return NULL;                   /* couldn't allocate memory */
    /*
     * Now write out the unfiltered scanline data.
     */
    p = scandata;
    for (i = 0; i < npasses; i++) {
        int shift = 16 - bitdepth;
#ifndef NDEBUG
        unsigned char *q = p;          /* only used for assertion */
#endif
        int x, y;

        if (!pass_width[i] || !pass_height[i])
            continue;                  /* skip this pass completely */

        for (y = passes[i].ystart; y < h; y += passes[i].ystep) {
            unsigned int bits = 0, nbits = 0;
#ifndef NDEBUG
            unsigned char *r = p;      /* only used for assertion */
#endif
            *p++ = 0;                /* filter type 0: currently unfiltered */
            for (x = passes[i].xstart; x < w; x += passes[i].xstep) {
                struct pixel pix;
                unsigned short samples[4];

                bp = bitmap + ((size_t)y * w + x) * channels;
                pix = read_pixel(&bp, channels, mul);

                /*
                 * Convert our pixel into a list of samples.
                 */
                switch (coltype) {
                  case 0:
                    samples[0] = pix.r >> shift;
                    break;
                  case 2:
                    samples[0] = pix.r >> shift;
                    samples[1] = pix.g >> shift;
                    samples[2] = pix.b >> shift;
                    break;
                  case 4:
                    samples[0] = pix.r >> shift;
                    samples[1] = pix.a >> shift;
                    break;
                  case 6:
                    samples[0] = pix.r >> shift;
                    samples[1] = pix.g >> shift;
                    samples[2] = pix.b >> shift;
                    samples[3] = pix.a >> shift;
                    break;
                  case 3:
                    for (j = 0; j < npalette; j++) {
                        struct pixel p1 = palette[j];
                        if (p1.r == pix.r && p1.g == pix.g &&
                            p1.b == pix.b && p1.a == pix.a) {
                            break;
                        }
                    }
                    assert(j < npalette);
                    samples[0] = j;
                    break;
                }
                /*
                 * Write the samples out into the bit stream.
                 */
                for (j = 0; j < ochannels; j++) {
                    bits = (bits << bitdepth) | samples[j];
                    nbits += bitdepth;
                    while (nbits >= 8) {
                        *p++ = bits >> (nbits-8);
                        nbits -= 8;
                    }
                }
            }

            /*
             * Output a partial byte if we have one.
             */
            if (nbits > 0) {
                *p++ = bits << (8-nbits);
            }

            assert(p - r == pass_width[i]);
        }
        assert(p - q == pass_width[i] * pass_height[i]);
    }
    assert(p - scandata == scandata_size);

    /*
     * Filter the scanline data.
     *
     * We work backwards along the data we've just written (so that
     * when we filter each scanline its predecessor is still
     * available in its untransformed state). For each scanline, we
     * compute all five filtered forms, and choose the one which
     * gives the smallest mean absolute value of all image bytes.
     */
    filtered[0] = p;
    for (i = 1; i < 4; i++)
        filtered[i] = filtered[i-1] + max_width;
    filter_offset = (ochannels * bitdepth) >> 3;
    if (filter_offset == 0)
        filter_offset = 1;
    for (i = npasses; i-- > 0 ;) {
        unsigned x, y;

        if (!pass_width[i] || !pass_height[i])
            continue;                  /* skip this pass completely */

        for (y = passes[i].ystart; y < h; y += passes[i].ystep) {
            unsigned char *scan = (p -= pass_width[i]);
            unsigned char *prev = scan - pass_width[i];
            unsigned sumabs[5];
            int filt;

            /*
             * For the topmost scanline of the pass, there is no
             * previous scanline to use in the filtering..
             */
            if (y + passes[i].ystep >= h)
                prev = NULL;

            for (j = 0; j < 4; j++)
                filtered[j][0] = j+1;
            for (j = 0; j < 5; j++)
                sumabs[j] = 0;

            for (x = 1; x < pass_width[i]; x++) {
                unsigned orig = scan[x];
                unsigned a = (x >= filter_offset ?
                              scan[x-filter_offset] : 0);
                unsigned b = (prev ? prev[x] : 0);
                unsigned c = (prev && x >= filter_offset ?
                              prev[x-filter_offset] : 0);
                unsigned p, pa, pb, pc, pr;

                filtered[0][x] = orig - a;
                filtered[1][x] = orig - b;
                filtered[2][x] = orig - ((a + b) >> 1);
                p = a + b - c;
                pa = uabs(p - a);
                pb = uabs(p - b);
                pc = uabs(p - c);
                if (pa <= pb && pa <= pc)
                    pr = a;
                else if (pb <= pc)
                    pr = b;
                else
                    pr = c;
                filtered[3][x] = orig - pr;

                sumabs[0] += abs((signed char)scan[x]);
                sumabs[1] += abs((signed char)filtered[0][x]);
                sumabs[2] += abs((signed char)filtered[1][x]);
                sumabs[3] += abs((signed char)filtered[2][x]);
                sumabs[4] += abs((signed char)filtered[3][x]);
            }

            filt = 0;
            for (j = 0; j < 4; j++)
                if (sumabs[j+1] < sumabs[filt])
                    filt = j+1;
            if (filt > 0)
                memcpy(scan, filtered[filt-1], pass_width[i]);
        }
    }
    assert(p == scandata);

    /*
     * Compress the bitmap data.
     */
    def = deflate_compress_new(DEFLATE_TYPE_ZLIB);
    deflate_compress_data(def, scandata, scandata_size, DEFLATE_END_OF_DATA,
                          &compressed, &complen);
    deflate_compress_free(def);
    free(scandata);

    /*
     * Construct the full output file.
     */
    filesize = 8;                      /* PNG header */
    filesize += 12+13;                 /* IHDR chunk */
    if (coltype == 3) {
        filesize += 12 + 3 * npalette; /* PLTE chunk */
        if (alpha) {
            filesize += 12 + npalette; /* tRNS chunk */
        }
    }
    filesize += 12+complen;            /* IDAT chunk */
    filesize += 12;                    /* IEND chunk */
    output = (unsigned char *)malloc(filesize);
    if (!output) {
        free(compressed);
        return NULL;
    }

    memcpy(output, "\x89PNG\x0D\x0A\x1A\x0A", 8);   /* PNG header */
    p = output + 8;

    p += 8; q = p;
    memcpy(q-4, "IHDR", 4);
    PUT_32BIT_MSB_FIRST(p, w);
    PUT_32BIT_MSB_FIRST(p+4, h);
    p[8] = bitdepth;
    p[9] = coltype;
    p[10] = 0;                         /* fixed compression method: zlib */
    p[11] = 0;                         /* fixed filter method */
    p[12] = 1;                         /* fixed interlace method: adam7 */
    p += 13;
    crc = crc32_update(0, q-4, p-q+4);
    PUT_32BIT_MSB_FIRST(q-8, p-q);
    PUT_32BIT_MSB_FIRST(p, crc);
    p += 4;

    if (coltype == 3) {
        p += 8; q = p;
        memcpy(p-4, "PLTE", 4);
        for (i = 0; i < npalette; i++) {
            *p++ = palette[i].r >> 8;
            *p++ = palette[i].g >> 8;
            *p++ = palette[i].b >> 8;
        }
        crc = crc32_update(0, q-4, p-q+4);
        PUT_32BIT_MSB_FIRST(q-8, p-q);
        PUT_32BIT_MSB_FIRST(p, crc);
        p += 4;

        if (alpha) {
            p += 8; q = p;
            memcpy(p-4, "tRNS", 4);
            for (i = 0; i < npalette; i++) {
                *p++ = palette[i].a >> 8;
            }
            crc = crc32_update(0, q-4, p-q+4);
            PUT_32BIT_MSB_FIRST(q-8, p-q);
            PUT_32BIT_MSB_FIRST(p, crc);
            p += 4;
        }
    }

    p += 8; q = p;
    PUT_32BIT_MSB_FIRST(p, 13);
    memcpy(p-4, "IDAT", 4);
    memcpy(p, compressed, complen);
    free(compressed);
    p += complen;
    crc = crc32_update(0, q-4, p-q+4);
    PUT_32BIT_MSB_FIRST(q-8, p-q);
    PUT_32BIT_MSB_FIRST(p, crc);
    p += 4;

    p += 8; q = p;
    PUT_32BIT_MSB_FIRST(p, 13);
    memcpy(p-4, "IEND", 4);
    /* this chunk is empty */
    crc = crc32_update(0, q-4, p-q+4);
    PUT_32BIT_MSB_FIRST(q-8, p-q);
    PUT_32BIT_MSB_FIRST(p, crc);
    p += 4;

    assert(p - output == filesize);
    *size = filesize;
    return output;
}

#ifdef TEST_PNGOUT

unsigned short test[256*256*4];

void write(void *data, size_t size, char *name)
{
    FILE *fp = fopen(name, "w");
    fwrite(data, size, 1, fp);
    fclose(fp);
}

int main(void)
{
    int x, y;
    unsigned short *p;
    size_t size;
    void *data;

    /* Paletted, greyscale, no alpha. */
    p = test;
    for (y = 0; y < 256; y++)
        for (x = 0; x < 256; x++)
            *p++ = ((x >> 5) + (y >> 5)) & 1;
    data = pngwrite(256, 256, 1, 1, test, &size);
    write(data, size, "test1.png");

    /* Paletted, colour, no alpha. */
    p = test;
    for (y = 0; y < 256; y++)
        for (x = 0; x < 256; x++) {
            *p++ = ((x >> 5) + (y >> 5)) & 1;
            *p++ = ~((x >> 5) + (y >> 5)) & 1;
            *p++ = 0;
        }
    data = pngwrite(256, 256, 3, 1, test, &size);
    write(data, size, "test2.png");

    /* Paletted, greyscale, alpha. */
    p = test;
    for (y = 0; y < 256; y++)
        for (x = 0; x < 256; x++) {
            *p++ = (((x >> 5) + (y >> 5)) & 1) << 1;
            *p++ = 1;
        }
    data = pngwrite(256, 256, 2, 2, test, &size);
    write(data, size, "test3.png");

    /* Paletted, colour, alpha. */
    p = test;
    for (y = 0; y < 256; y++)
        for (x = 0; x < 256; x++) {
            *p++ = (((x >> 5) + (y >> 5)) & 1) << 1;
            *p++ = (~((x >> 5) + (y >> 5)) & 1) << 1;
            *p++ = 0;
            *p++ = 1;
        }
    data = pngwrite(256, 256, 4, 2, test, &size);
    write(data, size, "test4.png");

    /* True-colour, greyscale, no alpha. */
    p = test;
    for (y = 0; y < 256; y++)
        for (x = 0; x < 256; x++)
            *p++ = x*256+y;
    data = pngwrite(256, 256, 1, 16, test, &size);
    write(data, size, "test5.png");

    /* True-colour, colour, no alpha. */
    p = test;
    for (y = 0; y < 256; y++)
        for (x = 0; x < 256; x++) {
            *p++ = x;
            *p++ = y;
            *p++ = 0;
        }
    data = pngwrite(256, 256, 3, 8, test, &size);
    write(data, size, "test6.png");

    /* True-colour, greyscale, alpha. */
    p = test;
    for (y = 0; y < 256; y++)
        for (x = 0; x < 256; x++) {
            *p++ = x*256+y;
            *p++ = 0x8000;
        }
    data = pngwrite(256, 256, 2, 16, test, &size);
    write(data, size, "test7.png");

    /* True-colour, colour, alpha. */
    p = test;
    for (y = 0; y < 256; y++)
        for (x = 0; x < 256; x++) {
            *p++ = x;
            *p++ = y;
            *p++ = 0;
            *p++ = 0x80;
        }
    data = pngwrite(256, 256, 4, 8, test, &size);
    write(data, size, "test8.png");

    return 0;
}

#endif
