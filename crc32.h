#ifndef CRC32_H
#define CRC32_H
unsigned long crc32_update(unsigned long crcword, const void *vdata, int len);
#endif /* CRC32_H */
