#ifndef PNGOUT_H
#define PNGOUT_H

void *pngwrite(int w, int h, int channels, int depth,
               const unsigned short *bitmap, size_t *size);

#endif /* PNGOUT_H */
