/* ----------------------------------------------------------------------
 * Declarations for functions to write out .BMP files.
 */

#ifndef BMPWRITE_H
#define BMPWRITE_H

typedef enum { BMP, PPM, PNG } imagetype;

imagetype infer_type(const char *progname, const char *type,
                     const char *filename);

struct Bitmap;
struct Bitmap *bmpinit(char const *filename, int width, int height,
                       imagetype type);
void bmppixel(struct Bitmap *bm,
              unsigned char r, unsigned char g, unsigned char b);
void bmpendrow(struct Bitmap *bm);
void bmpclose(struct Bitmap *bm);

#endif /* BMPWRITE_H */
